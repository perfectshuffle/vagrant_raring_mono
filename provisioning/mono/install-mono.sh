#!/bin/bash

apt-get update
apt-get -y install software-properties-common
add-apt-repository -y ppa:v-kukol/mono-testing
apt-get update

apt-get -y install mono-runtime mono-runtime-sgen mono-devel

if [ ! -f /usr/bin/fsharpi ];
then
    echo "F# not installed. Downloading..."
	
    mkdir /tmp/fsharp
    cd /tmp/fsharp
    wget http://launchpadlibrarian.net/138655550/fsharp_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655551/fsharp-console_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655552/libfsharp-build4.3-cil_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655554/libfsharp-compiler-interactive-settings4.3-cil_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655555/libfsharp-compiler-server-shared4.3-cil_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655553/libfsharp-compiler4.3-cil_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655556/libfsharp-core4.3-cil_3.0.26-1_all.deb
    wget http://launchpadlibrarian.net/138655557/libfsharp-data-typeproviders4.3-cil_3.0.26-1_all.deb		
	dpkg --install *.deb
	cp -p /usr/lib/cli/FSharp.*-4.3/* /usr/lib/mono/4.0/
fi

exit 0
